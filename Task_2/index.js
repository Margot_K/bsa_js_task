function expect() {
  setTimeout(() => {
      var bubbleCollection = document.getElementsByClassName("bubble");
      var score = document.getElementById('score');

      for (var bubble of bubbleCollection ) {
        bubble.click();
      }
      console.log(bubbleCollection.length == score.innerText9);
  }, 5000)
}

function superSort(sequence) {
  return sequence
      .split(' ')
      .sort((a, b) => a.replace(/[0-9]/g, '') > b.replace(/[0-9]/g, '') ? 1 : -1)
      .reduce((p, c) => p + " " + c);
}

function compareTwoDates(firstDateString, secondDateString) {
  return new Date(firstDateString).getTime() === new Date(secondDateString).getTime()
}

function findDuplicate(sequence) {
  let duplicate = 0;

  if(sequence[0] == sequence[1]) {
      duplicate = sequence[0];
  }
  else if(sequence[0] == sequence[2]){
      duplicate = sequence[0];
  }
  else {
      duplicate = sequence[1];
  }

  return sequence.find(x => x !== duplicate);
}


function getDiscount(number, priceToPay) {
  if(number >= 10) {
      return priceToPay * 0.9;
  }
  if(number >= 5 && number < 10) {
      return priceToPay * 0.95;
  }
  return priceToPay;
}

function getDaysForMonth(month) {
  switch(month) {
      case 4:
      case 6:
      case 9:
      case 11:
          return 30;
      case 2:
          return 28
      default:
          return 31;
  }
}